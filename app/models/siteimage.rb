# encoding: utf-8
class Siteimage < ActiveRecord::Base
   has_attached_file :image,
                    :styles => {
                      :thumb => ["128x128>", :jpg],
                      :edit  => ["770x770>", :jpg],
                    },
                    :default_style => :thumb
   validates_attachment :image, :present => true,
   :content_type => { :content_type => ["image/jpg","image/jpeg", "image/gif", "image/png"] }
   validates            :image, :attachment_presence => true



require 'RMagick'

attr_accessor :x1, :y1, :width, :height

def my_crop(att)
  scaled_img = Magick::ImageList.new(self.image.path(:edit))
  orig_img = Magick::ImageList.new(self.image.path(:original))
  scale = orig_img.columns.to_f / scaled_img.columns
 
  args = [ att[:x1], att[:y1], att[:width], att[:height] ]
  args = args.collect { |a| a.to_i * scale }
 
  orig_img.crop!(*args)
  orig_img.write(self.image.path(:original))
 
  self.image.reprocess!
  self.save
end
end
  append_translations <<STR
ru:
  activerecord:
    attributes:
      siteimage:
        image: "Изображение"
        x1: "x"
        y1: "y"
        width: "Ширина"
        height: "Высота"
    models:
      siteimage:
        zero:  "Изображений"
        one:   "Изображение"
        few:   "Изображения"
        many:  "Изображений"
        other: "Изображений"
  placeholder:
    siteimage:
      image: "Выберите файл изображения"
      x1: "Введите координату x"
      y1: "Введите координату y"
      width: "Введите ширину"
      height: "Введите высоту"
  confirm_delete:
      siteimage: 'Вы действительно хотите удалить изображение %{title}?'
STR
