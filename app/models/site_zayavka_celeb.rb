# encoding: utf-8
class SiteZayavkaCeleb
  include ActiveModel::Validations
  include ActiveModel::Conversion
  extend ActiveModel::Naming
  include Validators
  def initialize(attributes={})
    attributes.each do |n,v| 
      send "#{n}=" , v
    end
  end
  def self.i18n_scope
    :activerecord
  end
  def persisted?
    false
  end
 
  attr_accessor :email, :target
  
  validates :target,     length: { minimum: 5 }
  validates :email, :on => :update, :'validators/email' => true  
end
append_translations <<STR
ru:
  activerecord:
    models:
      site_zayavka_celeb:
        zero:  "Сообщений"
        one:   "Сообщение"
        few:   "Сообщения"
        many:  "Сообщений"
        other: "Сообщений"        
    attributes:
      site_zayavka_celeb:
        target:  "Кого Вы хотите поздравить"
        email: "е-майл"
  placeholder:
      site_zayavka_celeb:
        target:  "Введите кого Вы хотите поздравить"
        email: "Введите Ваш е-майл"
STR
