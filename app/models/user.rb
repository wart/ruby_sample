# encoding: utf-8
class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, 
         :recoverable, :rememberable, :trackable, :validatable, :registerable      
         
  #attr_accessible :email
end
  append_translations <<STR
ru:
  activerecord:
    models:
      user:
        zero:  "Пользователь"
        one:   "Пользователь"
        few:   "Пользователя"
        many:  "Пользователей"
        other: "Пользователей"
    attributes:
      user:
        password: "Пароль"
        password_confirmation: "Пароль(подтверждение)"
        email: " e-mail"
        current_password: "Текущие пароль"
  placeholder:
    user:
        password: "Введите ваш текущий пароль"
        email: "Введите ваш e-mail"
        password_confirmation: "Введите пароль еще раз"
        current_password: "Введите ваш текущий пароль"
STR