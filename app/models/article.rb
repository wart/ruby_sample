# encoding: utf-8
class Article < ActiveRecord::Base
  validates :title, length: { minimum: 5 }
  validates :text,  length: { minimum: 20 }
=begin  
  after_create :reindex!
  after_update :reindex!  
    
  searchable do
    text :title, :text
    integer :id
  end
=end  
end
append_translations <<STR
ru:
  activerecord:
    models:
      article:
        zero:  "Статей"
        one:   "Статья"
        few:   "Статьи"
        many:  "Статей"
        other: "Статей"
    attributes:
      article:
        title:  "Заголовок"
        text:   "Содежимое"
        publish: "Видимость"
        layout: "Макет"
        use_template: "Использовать шаблон"
  placeholder:
      article:
        title:  "Введите заголовок статьи"
        text:   "Введите текст статьи"
        publish: "Флаг видимости статьи для посетителей"
        layout: "Введите макет (стандартный application)"
        use_template: "Использовать шаблон"
  confirm_delete:
    article: 'Вы действительно хотите удалить статью %{title}?'
STR