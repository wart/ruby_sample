# encoding: utf-8
class Novost < ActiveRecord::Base
  validates :title, length: { minimum: 5 }
  validates :text,  length: { minimum: 20 }  
     scope :last5, ->{select(:id, :title, :text,:created_at).where(:publish=>true).order("created_at DESC").limit(5)}
=begin
  after_create :reindex!
  after_update :reindex!

  searchable do
    text :title, :text
    integer :id
    time    :created_at
#    string :sort_title do
#      title.downcase.gsub(/^(an?|the)/, '')
#    end
  end
   
  protected
    def reindex!
      Sunspot.index!(self)
    end
=end
end    
append_translations <<STR
ru:
  activerecord:
    models:
      novost:
        zero:  "Новостей"
        one:   "Новость"
        few:   "Новости"
        many:  "Новостей"
        other: "Новостей"
    attributes:
      novost:
        title:  "Заголовок"
        text:   "Содежимое"
        publish: "Видимость"
        urlname: "URL"
  placeholder:
      novost:
        title:  "Введите заголовок страницы"
        text:   "Введите текст страницы"
        publish: "Флаг видимости новости для посетителей"
        urlname: "URL адрес страницы"
  confirm_delete:
    novost: 'Вы действительно хотите удалить новость %{title}?'
STR
    