# encoding: utf-8
class SiteZayavkaMsg
  include ActiveModel::Validations
  include ActiveModel::Conversion
  extend ActiveModel::Naming
  include Validators
  def initialize(attributes={})
    attributes.each do |n,v| 
      send "#{n}=" , v
    end
  end
  def self.i18n_scope
    :activerecord
  end
  def persisted?
    false
  end
 
  attr_accessor :name, :phone, :email, :msg
  
  validates :name,    length: { minimum: 3 }
  validates :phone  , length: { minimum: 6 }
  validates :email, :on => :update, :'validators/email' => true
  validates :msg,     length: { minimum: 10 }
  
end
append_translations <<STR
ru:
  activerecord:
    models:
      site_zayavka_msg:
        zero:  "Сообщений"
        one:   "Сообщение"
        few:   "Сообщения"
        many:  "Сообщений"
        other: "Сообщений"        
    attributes:
      site_zayavka_msg:
        name:  "Ваше имя"
        email: "е-майл"
        phone: "Контактный телефон"
        msg:   "Текст сообщения"
  placeholder:
      site_zayavka_msg:
        name:  "Введите ваше имя"
        email: "Введите е-майл"
        phone: "Введите телефон"
        msg:   "Введите текст сообщения"
STR
  