# encoding: utf-8
class Paper < ActiveRecord::Base
  validates :title, length: { minimum: 3 }
  validates :text,  length: { minimum: 3 }
    
  scope :byName, ->(name) { where( urlname: name).first }

  def title=(val)
    super
    self.urlname=val.parameterize
  end
end  
  append_translations <<STR
ru:
  activerecord:
    models:
      paper:
        zero:  "Страниц"
        one:   "Страница"
        few:   "Страницы"
        many:  "Страниц" 
        other: "Страниц"
    attributes:
      paper:
        title:  "Заголовок"
        text:   "Содежимое"
        urlname: "URL"

  placeholder:
      paper:
        title:  "Введите заголовок страницы"
        text:   "Текст страницы"
        urlname: "URL адрес страницы"
  confirm_delete:
    paper: 'Вы действительно хотите удалить страницу %{title}?'
STR

