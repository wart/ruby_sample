# encoding: utf-8
class Issue < ActiveRecord::Base
  belongs_to :cover ,class_name: "Issuepage"
  has_many :issuepages, :dependent => :destroy
  validates :caption, length: { minimum: 5 }
  #validates :text,  length: 
=begin  
  after_create :reindex!
  after_update :reindex!  
    
  searchable do
    text :caption, :text
    integer :id
  end
=end  
  
  def destroy
    self.cover=nil
    self.cover_id=nil
    self.save
    self.issuepages.each do |i|
       i.destroy
    end 
    super
  end
=begin  
  protected
    def reindex!
      Sunspot.index!(self)
    end
=end           
end
append_translations <<STR
ru:
  activerecord:
    models:
      issue:
        zero:   "Номеров"
        one:    "Номер"
        few:    "Номера"
        many:   "Номеров"
        other:  "Номеров"
    attributes:
      issue:
        caption:  "Заголовок"
        text:     "Содежимое"
  placeholder:
    issue:
      caption:  "Введите заголовок издания"
      text:     "Введите описание издания"
  confirm_delete:
    issue:  "Вы действительно хотите удалить номер %{title}?"
  button:
    go_issue_pages:      'Страницы'
    go_issue_page_add:   'Добавить страницу'
    go_issue_list:       'К списку номеров'
    go_issue:            'К номеру'
STR