# encoding: utf-8
class Partner < ActiveRecord::Base
  validates :title, length: { minimum: 5 }
  validates :html,  length: { minimum: 20 }
end
append_translations <<STR
ru:
  activerecord:
    models:
      partner:
        zero:  "Партнеров"
        one:   "Партнер"
        few:   "Партнера"
        many:  "Партнеров"
        other: "Партнеров"
    attributes:
      partner:
        title:  "Заголовок"
        html:   "HTML код"
  placeholder:
    partner:
      title: "Введите название партнера"
      html:  "Введите HTML код отображения партнера"
  confirm_delete:
    partner: 'Вы действительно хотите удалить партнера %{title}?'
STR