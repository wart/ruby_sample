# encoding: utf-8
class Issuepage < ActiveRecord::Base
  belongs_to :issue

   has_attached_file :image,
                     :styles =>
                     {
                       :thumb => ["64x81>"  , :jpg],
                       :cover => ["110x140>", :jpg],
                       :page  => ["500x636>", :jpg]
                     },
                     :default_url   => '/theme/nocver.png',
                     :default_style => :thumb
   validates_attachment :image,
                        :present => true,
                        :content_type =>
                        {
                           :content_type => ["image/jpg","image/jpeg" ,"image/gif", "image/png"]
                        }  
                        
   validates            :image, :attachment_presence => true
                         
                        
#  scope :byissue , -> (issue) { where(:issue_id => issue.id ).order("created_at DESC") }
  
 # :last5, -> {select(:id, :title, :text,:created_at).where(:publish=>true).order("created_at DESC").limit(5)}
  
#  scope :created_before, ->(time) { where("created_at < ?", time) }
end
append_translations <<STR
ru:
  activerecord:
    models:
      issuepage:
        zero:  "Страниц"
        one:   "Страница"
        few:   "Страницы"
        many:  "Страниц"
        other: "Страниц"
    attributes:
      issuepage:
        note:    "Описание"
        image:  "Обложка"
  placeholder:
      issuepage:
        note:   "Введите описание страницы"
        image:  "Выберите файл изображения"
  confirm_delete:
    issuepage: 'Вы действительно хотите удалить страницу %{title}?'
STR