# encoding: utf-8
class SiteZayavka < ActionMailer::Base
  default from: 'censored',
          to: 'censored',
          copy: 'censored'
          
  def ads(msg)
    @msg = msg
    mail(subject: 'Сайтег: Заявка на рекламу')
  end  

  def ideya(msg)
    @msg = msg
    mail(subject: 'Сайтег: Есть идейка')
  end  

  def akciya1(msg)
    @msg = msg
    mail(subject: 'Сайтег: Запрос по акции')
  end  

  
end
