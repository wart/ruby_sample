class IssuepagesController < ApplicationController
  
  before_action :set_Issue, only: [:index, :create, :new]
  
  def index
    @items=@issue.issuepages.page(params[:page])
  end
  def new
    @item = @issue.issuepages.new
  end
 
  def create
    if user_signed_in?
      @item = @issue.issuepages.new(params.require(:issuepage).permit(:image,:note))
      if @item.save
        redirect_to @issue, notice: 'Item was successfully created.' 
      else
        render action: 'new'
      end        
    else
      notauthorized
    end
  end
  
  def destroy
    if user_signed_in?
      @issuepage = Issuepage.find(params[:id])
      @issue = @issuepage.issue
      if @issue.cover_id == @issuepage.id
        @issue.cover=nil
        @issue.save
      end
      @issuepage.destroy
      index
      render :index
    else
      notauthorized
    end    
  end
  
  def cover
    if user_signed_in?
      @issuepage = Issuepage.find(params[:id])
      @issue = @issuepage.issue
      @issue.cover = @issuepage
      @issue.save
      index
      render :index
    else
      notauthorized
    end    
  end
  
  private
    def set_Issue
      @issue = Issue.find(params[:issue_id])
    end
    def notauthorized
      render :text => '401 Unauthorized', :status => :unauthorized
    end
end
