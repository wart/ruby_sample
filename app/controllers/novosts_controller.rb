class NovostsController < ApplicationController
  model  Novost

  permit_params :id, :title, :text, :publish



  def index
    @items = model.select(:id, :title, :text,:created_at).where(:publish=>true).order("created_at DESC").page(params[:page])  #params[:page]
  end


  def index_adm
    @items = model.select(:id, :title, :text).page(params[:page])  #params[:page]
  end
end
 