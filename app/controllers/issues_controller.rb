class IssuesController < ApplicationController
  layout "issue"
  model         Issue
  index_fields   :id, :caption, :text, :cover_id
  permit_params  :id, :caption, :text, :cover_id
  index_includes :cover



  def main
    @item = model.last
    render "show"
  end 
 
 
end
