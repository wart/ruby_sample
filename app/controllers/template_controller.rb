class TemplateController < ApplicationController
  def index
    render text: File.read(File.expand_path('../../views/template/'+params[:arg].gsub(/(\.+)/,"."), __FILE__))
  end
end