class ArticlesController < ApplicationController
  model  Article
#  layout :get_layout
  permit_params :id, :title, :text, :publish,:layout,:use_template
  
  def index
    @items = model.select(:id, :title, :text).where(:publish=>true).page(params[:page])  #params[:page]
  end


  def index_adm
    @items = model.select(:id, :title, :text).page(params[:page])  #params[:page]
  end  
  
  def show
    if @item.use_template?
     render 'show',layout: @item.layout
    else
      render 'show2',layout: @item.layout
    end
     
  end
=begin  
  private
    def get_layout
      @item.layout
    end
=end  
end
