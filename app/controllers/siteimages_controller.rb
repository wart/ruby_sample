class SiteimagesController < ApplicationController
  before_action :set_siteimage, only: [:show, :edit, :update, :destroy]

  def index
    @items = Siteimage.all.order("id desc").page(params[:page]) 
  end
  def new
    @item = Siteimage.new
  end
  def edit
  end
  def show
  end
#--------------------------------------------------------------  
   def create
    if user_signed_in?
      @item = Siteimage.new(siteimage_params)
      respond_to do |format|
        if @item.save
          format.html { redirect_to @item, notice: 'Siteimage was successfully created.' }
        else
          format.html { render action: 'new' }
        end
      end
    else
      notauthorized
    end
  end

  def update
    if user_signed_in?
      params.require(:siteimage).permit(:x1,:y1,:width,:height,:image)
      if @item.my_crop params[:siteimage]
        flash[:notice] = 'Upload was successfully updated.'
        redirect_to @item
      else
        render :action => "edit"
      end
    else
      notauthorized
    end
  end


  def destroy
    if user_signed_in?
      @item.destroy
      respond_to do |format|
        format.html { redirect_to siteimages_url }
        format.json { head :no_content }
      end
    else
      notauthorized
    end
  end

  private
  
    def set_siteimage
      @item = Siteimage.find(params[:id])
    end
    
    def siteimage_params
      params.require(:siteimage).permit(:image)
    end
    def notauthorized
      render :text => '401 Unauthorized', :status => :unauthorized
    end    
end
