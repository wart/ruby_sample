class FeedbackController < ApplicationController
  before_action :new_item,  only: [:ads     , :ideya]
  before_action :post_item, only: [:ads_post, :ideya_post]

  def ads
  end

  def ideya
  end
  
  def akciya1
    @msg = SiteZayavkaCeleb.new
  end

  def ads_post
    b=@msg.valid?
    a=verify_recaptcha(:model => @msg)
    
    if a&&b
      SiteZayavka.ads(@msg).deliver
      redirect_to root_path, notice: t(:post_done)
    else
      render :ads
    end
  end

  def ideya_post
    b=@msg.valid?
    a=verify_recaptcha(:model => @msg)
    if a&&b
      SiteZayavka.ideya(@msg).deliver
      redirect_to root_path, notice: t(:post_done)
    else
      render :ideya 
    end
  end
  
  def akciya1_post
    @msg = SiteZayavkaCeleb.new(params.require(:site_zayavka_celeb).permit(:email,:target))
    b=@msg.valid?
    a=verify_recaptcha(:model => @msg)
    if a&&b
      SiteZayavka.akciya1(@msg).deliver
      redirect_to root_path, notice: t(:post_done)
    else
      render :akciya1 
    end
  end
#----------------------------------------------------------------------
  private
    def new_item
      @msg = SiteZayavkaMsg.new
    end
    def post_item
      @msg = SiteZayavkaMsg.new(params.require(:site_zayavka_msg).permit(:name,:phone,:email,:msg))
    end
end