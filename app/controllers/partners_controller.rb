class PartnersController  < ApplicationController
  model  Partner
  index_fields :id, :title, :html
  permit_params :id, :title, :html

  end
