class PapersController < ApplicationController
  model  Paper
  index_fields  :id, :title, :urlname
  permit_params :id, :title, :text, :inmenu

  def byname
    @item = model.byName(params[:urlname])
    render "show"
  end




end
