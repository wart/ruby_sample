# encoding: utf-8
class SearchController < ApplicationController


 def index
    @items = Paper.search {
      keywords params[:arg]
     # order_by :title,:asc
    }.results
    render 'papers/index'
  end


=begin
  def index
    models = []
    Dir.glob('./app/models/*.rb').each { |file| require file }
    models = ActiveRecord::Base.send(:subclasses)
    @models=models.select { |f| f.respond_to? :search }
    @res=[]
    @models.each do |m|
      @res=@res+ m.send(:all)      
    end
    @pattern=params[:arg]
  end
=end  
  

end
