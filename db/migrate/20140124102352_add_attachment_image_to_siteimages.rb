class AddAttachmentImageToSiteimages < ActiveRecord::Migration
  def self.up
    change_table :siteimages do |t|
      t.attachment :image
    end
  end

  def self.down
    drop_attached_file :siteimages, :image
  end
end
