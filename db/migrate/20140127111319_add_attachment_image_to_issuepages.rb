class AddAttachmentImageToIssuepages < ActiveRecord::Migration
  def self.up
    change_table :issuepages do |t|
      t.attachment :image
    end
  end

  def self.down
    drop_attached_file :issuepages, :image
  end
end
