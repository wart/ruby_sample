class CreateIssuepages < ActiveRecord::Migration
  def change
    create_table :issuepages do |t|
      t.belongs_to :issue, :null => false, index: true
      t.string :note, :null=>true
    end
    execute "ALTER TABLE issuepages ADD CONSTRAINT fk_issuepages_issue   FOREIGN KEY (issue_id) REFERENCES issues(id)"
  end
end
