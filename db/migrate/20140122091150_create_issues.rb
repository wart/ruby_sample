class CreateIssues < ActiveRecord::Migration
  def change
    create_table :issues do |t|
      t.string :caption
      t.text   :text
      t.belongs_to :cover , class_name: "Issuepage", :null => true, index: true      
    end
  end
end
