class CreatePartner < ActiveRecord::Migration
  def change
    create_table :partners do |t|
      t.string :title
      t.text :html
    end
  end
end
