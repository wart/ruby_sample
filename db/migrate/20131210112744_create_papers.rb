# encoding: utf-8
class CreatePapers < ActiveRecord::Migration
  def change
    create_table :papers do |t|
      t.string :title,:null => false
      t.text :text,:null => false
      t.string :urlname,:null => false
    end
    Paper.create(title: "Сотрудничество"         , text: "Сотрудничество")
    Paper.create(title: "Прайс"                  , text: "Прайс")
    Paper.create(title: "Специальное предложение", text: "Специальное предложение")
    Paper.create(title: "Контакты"               , text: "Контакты")
  end
end
