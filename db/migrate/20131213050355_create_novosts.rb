class CreateNovosts < ActiveRecord::Migration
  def change
    create_table :novosts do |t|
      t.string :title
      t.text :text
      t.boolean :publish
      t.string :urlname

      t.timestamps
    end
  end
end
