class AddFieldToArticles < ActiveRecord::Migration
  def change
    add_column :articles, :layout, :string
    add_column :articles, :use_template, :boolean
  end
end
