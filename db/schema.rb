# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20140407032320) do

  create_table "articles", force: true do |t|
    t.string  "title"
    t.text    "text"
    t.boolean "publish"
    t.string  "layout"
    t.boolean "use_template"
  end

  create_table "issuepages", force: true do |t|
    t.integer  "issue_id",           null: false
    t.string   "note"
    t.string   "image_file_name"
    t.string   "image_content_type"
    t.integer  "image_file_size"
    t.datetime "image_updated_at"
  end

  add_index "issuepages", ["issue_id"], name: "index_issuepages_on_issue_id", using: :btree

  create_table "issues", force: true do |t|
    t.string  "caption"
    t.text    "text"
    t.integer "cover_id"
  end

  add_index "issues", ["cover_id"], name: "index_issues_on_cover_id", using: :btree

  create_table "novosts", force: true do |t|
    t.string   "title"
    t.text     "text"
    t.boolean  "publish"
    t.string   "urlname"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "papers", force: true do |t|
    t.string "title",   null: false
    t.text   "text",    null: false
    t.string "urlname", null: false
  end

  create_table "partners", force: true do |t|
    t.string "title"
    t.text   "html"
  end

  create_table "siteimages", force: true do |t|
    t.string   "image_file_name"
    t.string   "image_content_type"
    t.integer  "image_file_size"
    t.datetime "image_updated_at"
  end

  create_table "users", force: true do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

  add_foreign_key "issuepages", "issues", name: "fk_issuepages_issue"

  add_foreign_key "issues", "issuepages", name: "fk_issues_cover", column: "cover_id"

end
