Ideya::Application.routes.draw do

  root 'title#index'
  
  devise_for :users, format: false
 
  resources :papers , except: [:show,:destroy],  format: false
  #get 'papers/:id/delete'   => 'papers#destroy',    as: :delete_paper, format: false
  get 'papers/page/:page'   => 'papers#index' ,     as: :paper_page,    format: false
  get 'papers/:id'          => 'papers#show'  ,                         format: false, constraints: { id: /\d*/ }
  get 'papers/:urlname'     => 'papers#byname',     as: :paper_byname,  format: false
  get 'paper_main'          => 'papers#main'  ,     as: :paper_main,    format:false
   
  resources :news , controller: :novosts, as: :novosts, format: false  , except: [:show,:destroy]
  get 'news/admin'          => 'novosts#index_adm', as: :novosts_adm,   format: false
  get 'news/:id/delete'     => 'novosts#destroy'  , as: :delete_novost, format: false
  get 'news/:id'            => 'novosts#show'     ,                     format: false, constraints: { id: /\d*/ }
  get 'news/page/:page'     => 'novosts#index'    , as: :novosts_page,  format: false
  
  resources :articles, except: [:show,:destroy],  format: false
  get 'articles/admin'      => 'articles#index_adm',as: :articles_adm,  format: false
  get 'articles/:id/delete' => 'articles#destroy' , as: :delete_article,format: false
  get 'articles/page/:page' => 'articles#index' ,   as: :articles_page, format: false
  get 'articles/:id'        => 'articles#show'  ,                       format: false, constraints: { id: /\d*/ }

  resources :partners, format: false  , except: [:show,:destroy]
  get 'partners/:id'        => 'partners#show',                         format: false, constraints: { id: /\d*/ }
  get 'partners/:id/delete' => 'partners#destroy',  as: :delete_partner,format: false

  resources :siteimages, format: false, except: [:destroy]
   get 'siteimages/:id/delete'   => 'siteimages#destroy'    , as: :delete_siteimage,  format: false
    
  resources :issues , controller: :issues, as: :issues, format: false  , except: [:show,:destroy,:update], shallow: true do
    resources :issuepages , controller: :issuepages, path: :papers,  format: false, except: [:show,:destroy,:update,:edit]  
  end
  get 'issues/:id'            => 'issues#show'        , as: :issue,            format: false, constraints: { id: /\d*/ }
  get 'issues/page/:page'     => 'issues#index'       , as: :issues_page,      format: false
  get 'issues/:id/delete'     => 'issues#destroy'     , as: :delete_issue,     format: false
  get 'issues/admin'          => 'issues#index_adm'   , as: :issues_adm,       format: false
  get 'issuepages/:id/delete' => 'issuepages#destroy' , as: :delete_issuepage, format: false
  get 'issues/:issue_id/page/:page' => 'issuepages#index'  , as: :issuepages_page,      format: false
  get 'issues/:issue_id/cover/:id' => 'issuepages#cover'  , as: :cover_issuepage,      format: false


  get  'feedback/ads'   => 'feedback#ads'        , as: :feedback_ads        , format: false
  post 'feedback/ads'   => 'feedback#ads_post'   , as: :post_feedback_ads   , format: false
  get  'feedback/ideya' => 'feedback#ideya'      , as: :feedback_ideya      , format: false
  post 'feedback/ideya' => 'feedback#ideya_post' , as: :post_feedback_ideya , format: false

  get  'feedback/akciya1' => 'feedback#akciya1'      , as: :feedback_akciya1      , format: false
  post 'feedback/akciya1' => 'feedback#akciya1_post' , as: :post_feedback_akciya1 , format: false

  get  'vk/albums' => 'vkphotos#index' , as: :vkalbums , format: false
  get  'vk/photos/:id' => 'vkphotos#photos'  , format: false


#  get  'search/*arg' => 'search#index', as: :search ,format: false #

  get 'template/*arg' => 'template#index', as: :template, format: false

  if Rails.env.production?
    get '404', :to => 'application#page_not_found'
    get '422', :to => 'application#server_error'
    get '500', :to => 'application#server_error'
  end

#  get 'otzivi'   => 'staticviews#ozivs'    , as: :otzivs_page, format: false

end
