require File.expand_path('../boot', __FILE__)
require 'rails/all'
Bundler.require(:default, Rails.env)
module Ideya
  class Application < Rails::Application
    config.autoload_paths += %W(#{config.root}/app/models/validators)
    config.i18n.default_locale = :ru
    config.i18n.locale = :ru    
    
    config.i18n.load_path += Dir[Rails.root.join('config', 'locales', '**', '*.{rb,yml}')]
    #puts config.i18n.load_path
    
    config.assets.enable=true
    Paperclip::Attachment.default_options[:url] = "/system/:class/:attachment/:id/:style.:extension"
    
    if Rails.env.production?
      config.exceptions_app = self.routes
    end
    
    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration should go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded.

    # Set Time.zone default to the specified zone and make Active Record auto-convert to this zone.
    # Run "rake -D time" for a list of tasks for finding time zone names. Default is UTC.
    # config.time_zone = 'Central Time (US & Canada)'

  end
end
