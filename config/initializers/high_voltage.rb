=begin
HighVoltage.configure do |config|
  config.route_drawer = HighVoltage::RouteDrawers::Root
  config.action_caching = true
  config.action_caching_layout = true # optionally do not cache layout. default is true.
  config.page_caching = true
end
=end