module I18n
  module Registry
    protected
    def lookup(locale, key, scope = [], options = {})
     #if key.to_s.downcase=="novost"
        @log ||= Logger.new(File.join(Rails.root, 'log', 'i18n_registry.log'))
        @log.info "#{key} - #{scope} - #{options}"
     #end
      super
    end
  end
end

#I18n::Backend::Simple.send :include, I18n::Registry
