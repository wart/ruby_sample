if defined? Bullet
  Bullet.enable = true # включаем в работу
  Bullet.alert = true # пусть подсказка по запросам "всплывает" прямо в браузере
end
