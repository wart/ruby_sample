# Load the Rails application.
require File.expand_path('../application', __FILE__)
# Initialize the Rails application.
Ideya::Application.initialize!
Ideya::Application.configure do
  config.action_mailer.delivery_method = :smtp
  config.action_mailer.raise_delivery_errors = true
  config.action_mailer.perform_deliveries = true
#  config.action_mailer.smtp_settings = {
   ActionMailer::Base.smtp_settings = {
    address:              'censored',
    port:                 25,
    domain:               'censored',
    user_name:            'support@censored',
    password:             'censored',
    authentication:       'plain',# 'login'
    enable_starttls_auto: false      
  }
  config.i18n.enforce_available_locales = false
end
ActionView::Base.default_form_builder = Wlad::Bootstrap::LabelFormBuilder
 
